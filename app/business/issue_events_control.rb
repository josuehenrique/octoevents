class IssueEventsControl
  def initialize(event)
    @event = event
  end

  def self.persist_event!(*attr)
    new(*attr).persist_event!
  end

  def persist_event!
    issue_event = IssueEvent.new
    issue_event.issue_number  = event['issue']['number']
    issue_event.action        = event['action']
    issue_event.issue         = event['issue']
    issue_event.issue_changes = event['changes']
    issue_event.repository    = event['repository']
    issue_event.sender        = event['sender']
    issue_event.save!
  end

  private

  attr_reader :event
end
