class IssueEvent < ApplicationRecord
  validates :issue_number, :action, :issue, :repository, :sender, presence: true

  scope :by_issue_number, -> value { where(issue_number: value) }

  def self.report(issue_number)
    IssueEvent.by_issue_number(issue_number).map do |event|
      {}.tap do |json|
        json['issue_number']  = event.attributes['issue_number']
        json['action']        = event.attributes['action']
        json['created_at']    = event.attributes['created_at']
        json['issue']         = event.attributes['issue']
        json['issue_changes'] = event.attributes['issue_changes']
        json['repository']    = event.attributes['repository']
        json['sender']        = event.attributes['sender']
      end
    end
  end
end
