class IssuesController < ApplicationController
  def events
    render json: IssueEvent.report(params[:issue_number])
  end
end
