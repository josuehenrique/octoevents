class ApplicationController < ActionController::API
  before_action :verify_signature

  def events
    IssueEventsControl.persist_event!(JSON.parse(request.body.read))
    head :ok
  end

  private

  def verify_signature
    return unless request.post?

    body = request.body.read
    sig = "sha1=#{OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), ENV['GITHUB_TOKEN'], body)}"

    head :unauthorized unless Rack::Utils.secure_compare(sig, request.env['HTTP_X_HUB_SIGNATURE'])
  end
end
