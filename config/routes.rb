Rails.application.routes.draw do
  post :events, controller: :application
  get '/issues/:issue_number/events', controller: :issues, action: :events
end
