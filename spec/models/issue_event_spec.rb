require 'rails_helper'

describe IssueEvent do
  it { should validate_presence_of(:issue_number) }
  it { should validate_presence_of(:action) }
  it { should validate_presence_of(:issue) }
  it { should validate_presence_of(:repository) }
  it { should validate_presence_of(:sender) }
end
