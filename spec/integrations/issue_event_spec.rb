require 'rails_helper'

describe IssueEvent do
  it do
    FactoryBot.create(:issue_event, issue_number: 3, action: 'unassigned')
    FactoryBot.create(:issue_event, issue_number: 3, action: 'assigned')
    FactoryBot.create(:issue_event, issue_number: 4, action: 'unlabeled')
    FactoryBot.create(:issue_event, issue_number: 3, action: 'closed')
    FactoryBot.create(:issue_event, issue_number: 5, action: 'demilestoned')

    expect(IssueEvent.report(3).pluck('action')).
      to eq(['unassigned', 'assigned', 'closed'])
  end
end
