FactoryBot.define do
  factory :issue_event do
    issue_number 10
    action 'opened'
    issue {{teste: 'teste'}}
    issue_changes {{teste: 'teste'}}
    repository {{teste: 'teste'}}
    sender {{teste: 'teste'}}
  end
end
