class CreateIssueEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :issue_events do |t|
      t.integer :issue_number, index: true, null: false
      t.string :action, null: false, index: true
      t.json :issue, null: false
      t.json :issue_changes
      t.json :repository, null: false
      t.json :sender, null: false

      t.timestamps
    end
  end
end
