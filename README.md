# README

###OctoEvents
This is a webhook for github issues.

* Ruby version **2.5.1**
* Rails version **5.2.0**
* PostgreSQL version **9.6**

* Setup

```ruby
bundle install 
rake db:create
rake db:migrate
export GITHUB_TOKEN=YOUR_CONFIGURED_WEBHOOK_TOKEN_ON_GITHUB
```

* To run 
```ruby
rails s -p 3000 -b 0.0.0.0
```

* How to run the test suite
```ruby
rspec spec
```

* List events by issue
```ruby
GET  /issues/:issue_number/events
```

* How to generate token for webhook?

```ruby
 ruby -rsecurerandom -e 'puts SecureRandom.hex(20)'
```

* CI/CD Link
```
https://app.wercker.com/josuehenrique/octoevents/runs
```

* Example

The server is hosted on [Heroku](https://octoevents-josuehenrique.herokuapp.com/)

The webhook was configured on [github url](https://github.com/josuehenrique/docker-ruby-chrome-headless) for issues actions (opened, edited, closed, reopened, assigned, unassigned, labeled, unlabeled, milestoned, or demilestoned).

To list event changes just `GET https://octoevents-josuehenrique.herokuapp.com/issues/:issue_id/events`